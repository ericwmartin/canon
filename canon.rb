# This script loops through all folders and subfolders in dir, and executes sort_idf function on each .idf file
# The IDD file for the version specified in the IDF must either be installed on the computer (in default directory), or passed as an argrument (e.g. modelkit execute canon-all-in-dir.rb C:/folder/Energy+.idd).
# passing only IDF version as agrument (e.g. modelkit execute canon-all-in-dir.rb 9.1) saves ~1 second of runtime, and also won't produce potential errors deriving from InputObject.rb ("Error:  Unidentified EnergyPlus class") if the IDF includes objects that DNE in temp_ep_version.
# if no argument is passed, find_temp_idd will execute to attempt to find the IDD in the latest EnergyPlus installation (temp_idd). temp_idd is used to grab actual version from IDF.
# if IDF version is different than temp_idd version, idd_parse will execute again with the orrect IDD version (the IDD version matching the IDF version).

# not implemented: (eppy) remove units, dashes, colons, *s, and other special characters. remove double zeros in scientific notations -- not implemented

require("modelkit/energyplus")
require('fileutils')

### Formattting Options
$idf_editor_style = false # format like IDF Editor
$no_units = false # remove units from field comments (eppy removes units)
$alphabetize = true # $alphabetize objects by name within each class
$fix_numbers = true # convert numbers from strings to float or int (i.e. remove extra zeros)

if $idf_editor_style
  $alphabetize = false
  $fix_numbers = false
  $no_units = false
end
###

# translate IDF version to EnergyPlus directory style, and adjust for intermediate versions if needed
# error will occur if IDF version is not in this table
$ep_version_table = {
"8.0" => "8-0-0",
"8.6" => "8-6-0",
"8.9" => "8-9-0",
"9.0" => "9-0-1", # this is the reason why this table is needed
"9.1" => "9-1-0",
"9.2" => "9-2-0",
"9.3" => "9-3-0",
"9.4" => "9-4-0",
}

def check_idd_filepath(idd_filepath)
  (File.file?(idd_filepath)) ? (return true) : (return false)
end

def check_idd_version(ver)
  if $ep_version_table.key?(ver)
    (RbConfig::CONFIG['host_os'] =~ /darwin|mac os/) ? (ep_folder = "/Applications/EnergyPlus-#{$ep_version_table[ver]}") : (ep_folder = "C:/EnergyPlusV#{$ep_version_table[ver]}")
    (File.file?(File.join(ep_folder,"Energy+.idd"))) ? (return true) : (return false)
  else
    return false
  end
end

def find_temp_idd # finds latest version of EnergyPlus installed and returns IDD filepath
  (RbConfig::CONFIG['host_os'] =~ /darwin|mac os/) ? ($app_dir = "/Applications/") : ($app_dir = "C:/")
  Dir.glob($app_dir + '*') do |f|
    @ep_dir = f if f.split("#{$app_dir}")[-1].to_s.include?("EnergyPlus")
  end
  idd_file = File.join(@ep_dir,"Energy+.idd")
  (File.file?(idd_file)) ? (return idd_file) : (return false)
end

# optionally pass version as argument (Usage: 'modelkit execute file.idf 9.0') instead of using first E+ IDD version (found in C:/ or /Applications/) to grab version from IDF
args = ARGV
if (not ARGV.empty?)
  args.each do |arg|
    if $ep_version_table.key?(arg)
      $arg_version = arg
    elsif arg.include? "Energy+.idd" # assume this is the path to the IDD
      $arg_idd = arg
    else
      abort("Error: invalid argument or specified version not found. Optional arguments: EPVersion (e.g. \"9.1\") or IDD path (e.g. \"C:/EnergyPlusV9-1-0/Energy+.idd\")") # this errors also occurs if version is not in ep_version_table
    end
  end
end

def idd_parse(idd_path)
  idd_ref = OpenStudio::DataDictionary.open(idd_path)
  classes = idd_ref.get_class_list
  # create a hash table with classes as keys, and their field numbers as values
  table = Hash.new { |hash, key| hash[key] = [] } # table = { Class_1: [object field number x, obj field number y], Class2:....}
  classes.each do |c|
    class_def = idd_ref.get_class_def(c)
    field_defs = class_def.field_definitions
    field_defs.shift  # Remove first item; it's blank
    field_defs.each_with_index do |f, i|
      table[c] << i # add field numbers to hash table as values
    end
  end
  return table
end

# extend String class for converting fields from strings to numbers
class String
  def is_number?
    true if Float(self) rescue false
  end
end

def sort_idf(idd, idf, class_table)
  if $idf_editor_style
    indent = "    " # field indentation below class name - eppy uses 2 spaces, IDF Editor uses 4 spaces
    rjust_col = 27 # start column for field comments (less 1) - eppy uses 42, IDF Editor uses 27
  else # if we want something different by default
    indent = "  "
    rjust_col = 27
  end
  # write new IDF to text
  text = "\n" # blank line at top of new IDF
  class_table.each do |key,values|
    if ((idf.find_objects_by_class_name(key.to_s).length() > 0))# && (key.to_s.include? "SizingPeriod:DesignDay"))## (key.to_s.include? for debugging) ## if class exists in IDF
      field_defs = idd.get_class_def(key.to_s).field_definitions # list of field definition hashes (first is blank because it is the class name itself)
      objects = idf.find_objects_by_class_name(key.to_s) # list of objects of that class in the IDF
      text << "\n!-   ===========  ALL OBJECTS IN CLASS: #{key.to_s.upcase} ===========\n\n" if $idf_editor_style
      # $alphabetize objects by name
      obj_names = []
      objects.each do |obj|
        obj_names << idf.find_object_by_class_and_name(key.to_s,obj.fields[1].to_s).to_s
      end
      obj_names = obj_names.sort if $alphabetize# sort alphabetically
      obj_names.each_with_index do |obj, n|
        # first find the last_field_num
        idf_obj = idf.find_object_by_class_and_name(key.to_s,obj_names[n].to_s)
        class_min_fields = idd.get_class_def(key).min_fields
        if idf_obj.fields.count > class_min_fields
          #find last non-blank field number
          for i in (class_min_fields..idf_obj.fields.count) # iterate from class min fields to number of fields in IDF object
            last_field_num = i if (idf_obj.fields[i].to_s != "")
          end
        end
        # iterate through object fields until last_field_num (break if num == last_field_num)
        idf_obj.fields.each_with_index do |field, num| # iterate over number of fields in IDF (or until num == last_field_num)
          if (num == 0) # first field (class name, no units)
            text << (field.to_s + ",\n")
            next
          elsif ( (num == (idf_obj.fields.count - 1) ) || (num == last_field_num) ) # last field
            field_end = ";"
            line_end = "\n\n"
          else # middle fields
            field_end = ","
            line_end = "\n"
          end
          if (field.to_s.is_number? && $fix_numbers) # convert strings to numbers if they represent numbers
            (field.to_f % 1 == 0) ? (field = field.to_i) : (field = field.to_f)
          end
          ((field.to_s.length < rjust_col - 2)) ? (spaces = "") : (spaces = "  ")
          if (key.to_s == "EnergyManagementSystem:Program")
            if num < 2 # .name method only defined for first field
              text << (indent + field.to_s + field_end + spaces + "!- ".rjust(rjust_col - field.to_s.length()) + field_defs[num].name.to_s + line_end)
            else # don't add field comments for lines in program
              text << (indent + field.to_s + field_end + spaces + line_end)
            end
          elsif (key.to_s == "EnergyManagementSystem:GlobalVariable") # .name method only defined for first 2 fields. better to simply label each field "Erl Variable #{num} Name"
            text << (indent + field.to_s + field_end + spaces + "!- ".rjust(rjust_col - field.to_s.length()) + "Erl Variable #{num} Name" + line_end)
          elsif (key.to_s == "LifeCycleCost:UsePriceEscalation") # .name method only defined for first 13 fields, no units
            if num < 14 # .name method only defined for first 13 fields
              text << (indent + field.to_s + field_end + spaces + "!- ".rjust(rjust_col - field.to_s.length()) + field_defs[num].name.to_s + line_end)
            else # don't add field comments for lines in program
              text << (indent + field.to_s + field_end + spaces + "!- ".rjust(rjust_col - field.to_s.length()) + "Year #{num-4} Escalation" + line_end)
            end
          elsif ( (field_defs[num].units_si.to_s == "") || (field_defs[num].units_si.to_s.include? "asedOnField") || ($no_units) ) # fields without units
            text << (indent + field.to_s + field_end + spaces + "!- ".rjust(rjust_col - field.to_s.length()) + field_defs[num].name.to_s + line_end)
          else # fields with units
            text << (indent + field.to_s + field_end + spaces + "!- ".rjust(rjust_col - field.to_s.length()) + field_defs[num].name.to_s + " {" + field_defs[num].units_si.to_s + "}" + line_end)
          end
          break if num == last_field_num
        end
      end
    end
  end
  return text
end

if $arg_version # if version passed as arg
  if check_idd_version($arg_version)
    (RbConfig::CONFIG['host_os'] =~ /darwin|mac os/) ? (ref_ep_dir = "/Applications/EnergyPlus-#{$ep_version_table[$arg_version]}") : (ref_ep_dir = "C:/EnergyPlusV#{$ep_version_table[$arg_version]}")
    temp_idd_path = File.join(ref_ep_dir,"Energy+.idd")
  else
    puts "Error:  IDD version passed as argument not found. Checking for other IDDs..."
    temp_idd_path = find_temp_idd()
    puts "IDD Found: #{temp_idd_path}" if File.file?(temp_idd_path)
  end
elsif $arg_idd # if IDD path passed as arg
  if check_idd_filepath($arg_idd)
    temp_idd_path = $arg_idd
  else
    puts "Error:  IDD path passed as argument not found. Checking for other IDDs..."
    temp_idd_path = find_temp_idd()
    puts "IDD Found: #{temp_idd_path}" if File.file?(temp_idd_path)
  end
else # no arguments passed -  check if any version of EnergyPlus is installed in default dirs
  temp_idd_path = find_temp_idd()
end
abort("Error: IDD not found and no EnergyPlus installation detected. Please specify IDD path as argument or install EnergyPlus in default directory (#{$app_dir}EnergyPlus-x-x-x).") if ((not temp_idd_path) || (not File.file?(temp_idd_path)))
temp_ep_version = temp_idd_path.split("-")[0][-1].to_s + "." + temp_idd_path.split("-")[1].to_s
# use temp_idd to parse IDD so that IDF version can be parsed from IDF
temp_idd = OpenStudio::DataDictionary.open(temp_idd_path)
temp_class_table = idd_parse(temp_idd_path)

# sort every IDF in dir
dir = File.expand_path(File.dirname(__FILE__))
Dir.glob(File.join(dir,"**", "*.idf")) do |filepath| # do for all IDFs in a directory
  next if (filepath.include? "_out.idf") # skip IDFs already sorted
  version = OpenStudio::InputFile.open(temp_idd, filepath).find_objects_by_class_name("Version")[0].to_s # read version from IDF
  if version.to_f == temp_ep_version.to_f
    class_table = temp_class_table
    idf = OpenStudio::InputFile.open(temp_idd, filepath)
    idd = temp_idd
  else # if IDF vesion != temp_ep_version, re-parse IDD and reset IDF with the actual version
    puts "EnergyPlus version #{version} detected in IDF. Parsing IDD version #{version}..."
    (RbConfig::CONFIG['host_os'] =~ /darwin|mac os/) ? (ref_ep_dir = "/Applications/EnergyPlus-#{$ep_version_table[version]}") : (ref_ep_dir = "C:/EnergyPlusV#{$ep_version_table[version]}")
    abort("Error: IDD version #{version} not detected on computer. Energy+.idd for EnergyPlus version #{version} is required to parse IDD.") if ( (not (defined?(ref_ep_dir))) || (not $ep_version_table.key?(version)))
    class_table =  idd_parse(File.join(ref_ep_dir,"Energy+.idd"))
    idd = OpenStudio::DataDictionary.open(File.join(ref_ep_dir,"Energy+.idd"))
    idf = OpenStudio::InputFile.open(idd, filepath)
  end
  puts "Sorting: #{filepath}..."
  outputfile = File.join(filepath.split(".idf")[0] + "_out.idf")
  File.write(outputfile, sort_idf(idd, idf, class_table))
  puts "Output: #{outputfile}"
end
